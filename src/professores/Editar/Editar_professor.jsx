import styles from "./editar_professor.module.css"
import { NavLink } from "react-router-dom"

export function Editar_professor() {
    return (
        <>
        <h1 className={styles.titulo}>Editar Professor(a)</h1>
        <section className={styles.formulario}>
            <form>
                <div>
                    <label>Nome</label>
                    <input type="text" required></input>
                </div>
                <div>
                    <label>CPF</label>
                    <input type="text" required></input>
                </div>
                <div>
                    <label>Email</label>
                    <input type="email" required></input>
                </div>
                <div>
                    <label>Senha</label>
                    <input type="password" required></input>
                </div>
                <div>
                    <NavLink to="/"><button type="submit" className={styles.botao1}>Cadastrar</button></NavLink>
                    <NavLink to="/"><button type="submit" className={styles.botao}>Cancelar</button></NavLink>
                </div>
            </form>
        </section>
        </>
    )
}