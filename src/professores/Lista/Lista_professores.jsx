import styles from "./lista_professores.module.css"
import { NavLink } from "react-router-dom"

export function Lista_Professores() {
    return (
        <>
        <section className={styles.titulo}>
            <h1>Professores</h1>
            <NavLink to="/Cadastrar_Professor"><button type="submit" className={styles.botao}>
                <img src="src/assets/plus-circle.svg" alt="adicionar"></img>
                <p>Professores</p>
            </button></NavLink>
        </section>
        <section className={styles.box}>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>User ID</th>
                        <th>CPF</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Darlene Robertson</td>
                        <td>2798</td>
                        <td>000.000.000-00</td>
                        <td>
                            <NavLink to="/Editar_Professor"><button className={styles.botao2}>Editar</button></NavLink>
                            <NavLink><button className={styles.botao2}>Excluir</button></NavLink>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Ronald Richard</td>
                        <td>4600</td>
                        <td>000.000.000-00</td>
                        <td>
                            <NavLink to="/Editar_Usuarios"><button className={styles.botao2}>Editar</button></NavLink>
                            <NavLink><button className={styles.botao2}>Excluir</button></NavLink>
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
        </>
    )
}