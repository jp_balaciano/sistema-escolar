import { NavLink } from "react-router-dom"
import styles from "./header.module.css"

export function Header() {
    return (
        <>
        <div className={styles.header}>
            <h1>Sistema Escola</h1>
            <section>
                <p>Nome do Usuário</p>
                <NavLink to="/login"><button type="submit" className={styles.botao}>Sair</button></NavLink>
            </section>
        </div>
        </>
    )
}