import React, { useState } from 'react';
import styles from "./drop_cadastro.module.css"

 export function Drop_cadastro() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false); 
  };

  return (
    <div className={styles.dropdown}>
      <button onClick={toggleDropdown} className={styles.dropbtn}>
        <section>
            {selectedOption ? selectedOption : 'Escolha um cargo'}
            <img src='src/assets/dropdown.svg'></img>
        </section>
      </button>
      {isOpen && (
        <div className={styles.dropdown_content}>
            <ul>
                <li onClick={() => handleOptionClick('Aluno')}>Aluno</li>
                <li onClick={() => handleOptionClick('Coordenador')}>Coordenador</li>
                <li onClick={() => handleOptionClick('Professor')}>Professor</li>
                <li onClick={() => handleOptionClick('Secretário')}>Secretário</li>
            </ul>
        </div>
      )}
    </div>
  );
}

