import React, { useState } from 'react';
import styles from "./drop_turno.module.css"

function Dropdown_turno() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false); 
  };

  return (
    <div className={styles.dropdown}>
      <button onClick={toggleDropdown} className={styles.dropbtn}>
        <section>
            {selectedOption ? selectedOption : 'Escolha o turno'}
            <img src='src/assets/dropdown.svg'></img>
        </section>
      </button>
      {isOpen && (
        <div className={styles.dropdown_content}>
            <ul>
                <li onClick={() => handleOptionClick('Manhã')}>Manhã</li>
                <li onClick={() => handleOptionClick('Tarde')}>Tarde</li>
                <li onClick={() => handleOptionClick('Noite')}>Noite</li>
            </ul>
        </div>
      )}
    </div>
  );
}

export default Dropdown_turno;