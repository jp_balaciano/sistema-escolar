import React, { useState } from 'react';
import styles from "./drop_disciplinas.module.css"

function Dropdown_disciplinas() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false); 
  };

  return (
    <div className={styles.dropdown}>
      <button onClick={toggleDropdown} className={styles.dropbtn}>
        <section>
            {selectedOption ? selectedOption : 'Escolha uma disciplina'}
            <img src='src/assets/dropdown.svg'></img>
        </section>
      </button>
      {isOpen && (
        <div className={styles.dropdown_content}>
            <ul>
                <li onClick={() => handleOptionClick('Biologia')}>Biologia</li>
                <li onClick={() => handleOptionClick('Filosofia')}>Filosofia</li>
                <li onClick={() => handleOptionClick('Física')}>Física</li>
                <li onClick={() => handleOptionClick('Geografia')}>Geografia</li>
                <li onClick={() => handleOptionClick('História')}>História</li>
                <li onClick={() => handleOptionClick('Língua Portuguesa')}>Língua Portuguesa</li>
                <li onClick={() => handleOptionClick('Matemática')}>Matemática</li>
                <li onClick={() => handleOptionClick('Química')}>Química</li>
            </ul>
        </div>
      )}
    </div>
  );
}

export default Dropdown_disciplinas;