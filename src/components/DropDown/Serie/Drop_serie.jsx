import React, { useState } from 'react';
import styles from "./drop_serie.module.css"

function Dropdown_serie() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false); 
  };

  return (
    <div className={styles.dropdown}>
      <button onClick={toggleDropdown} className={styles.dropbtn}>
        <section>
            {selectedOption ? selectedOption : 'Escolha a série'}
            <img src='src/assets/dropdown.svg'></img>
        </section>
      </button>
      {isOpen && (
        <div className={styles.dropdown_content}>
            <ul>
                <li onClick={() => handleOptionClick('1ª série')}>1ª série</li>
                <li onClick={() => handleOptionClick('2ª série')}>2ª série</li>
                <li onClick={() => handleOptionClick('3ª série')}>3ª série</li>
                <li onClick={() => handleOptionClick('4ª série')}>4ª série</li>
                <li onClick={() => handleOptionClick('5ª série')}>5ª série</li>
                <li onClick={() => handleOptionClick('6ª série')}>6ª série</li>
                <li onClick={() => handleOptionClick('7ª série')}>7ª série</li>
            </ul>
        </div>
      )}
    </div>
  );
}

export default Dropdown_serie;