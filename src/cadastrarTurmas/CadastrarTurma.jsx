import Dropdown_disciplinas from '../components/DropDown/Disciplinas/Drop_disciplinas'
import Dropdown_serie from '../components/DropDown/Serie/Drop_serie'
import Dropdown_turno from '../components/DropDown/Turno/Drop_turno'
import styles from './cadastrarturma.module.css'
import {  NavLink } from 'react-router-dom'
import { PlusCircle } from 'phosphor-react'


export function CadastrarTurma () {
    return(
        <div className={styles.center}>
            <div className={styles.container}>
            <h1>Cadastrar Turma</h1>
            <div>
                    <section className={styles.content1}>
                        <div className={styles.ano}>
                            <label className={styles.cadastraLabel}>Ano</label>
                            <input type="number" name="" id="" required />
                        </div>
                        <div className={styles.turno}>
                            <label htmlFor="turno" className={styles.cadastraLabel}>Turno</label>
                            <Dropdown_turno />
                        </div>
                        <div className={styles.serie}>
                            <label htmlFor="serie" className={styles.cadastraLabel}>Série</label>
                            <Dropdown_serie />
                        </div>
                    </section>

                    <section className={styles.discipline}>
                        <h2>Disciplinas</h2>
                        <div className={styles.content2}>
                            <Dropdown_disciplinas />
                            <button type="submit" >
                                <PlusCircle size={24}/>
                                <span>Disciplina</span>
                            </button>
                        </div>
                        <h2>Disciplinas Relacionadas</h2>
                        <div className={styles.disciplineSelecionadas}>
                            <p>Disciplina exemplo 1 - Cód. 000</p>
                            <button type="submit" className={styles.butRemove}>Remover</button>
                        </div>
                    </section>

                    <section className={styles.contentButton}>
                    <NavLink to='/turmas'>
                            <button type="submit" className={styles.butRegister}>Cadastrar</button>
                    </NavLink>
                    <NavLink to='/turmas'>
                            <button type="submit" className={styles.butCancel}>Cancelar</button>
                    </NavLink>
                    </section>
            </div>
            </div>
        </div>
    )
}