import styles from "./lista_usuarios.module.css"
import { NavLink } from "react-router-dom"

export function Lista_Usuarios() {
    return (
        <>
        <section className={styles.titulo}>
            <h1>Usuários</h1>
            <NavLink to="/Cadastrar_Usuarios"><button type="submit" className={styles.botao}>
                <img src="src/assets/plus-circle.svg" alt="adicionar"></img>
                <p>Usuário</p>
            </button></NavLink>
        </section>
        <section className={styles.box}>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>User ID</th>
                        <th>CPF</th>
                        <th>Cargo</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Darlene Robertson</td>
                        <td>2798</td>
                        <td>000.000.000-00</td>
                        <td>Coordenador(a)</td>
                        <td>
                            <NavLink to="/Editar_Usuarios"><button className={styles.botao2}>Editar</button></NavLink>
                            <NavLink><button className={styles.botao2}>Excluir</button></NavLink>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Ronald Richard</td>
                        <td>4600</td>
                        <td>000.000.000-00</td>
                        <td>Professor(a)</td>
                        <td>
                            <NavLink to="/Editar_Usuarios"><button className={styles.botao2}>Editar</button></NavLink>
                            <NavLink><button className={styles.botao2}>Excluir</button></NavLink>
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
        </>
    )
}