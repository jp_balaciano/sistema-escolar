import { Drop_cadastro } from "../../components/DropDown/Cadastro/Drop_cadastro"
import styles from "./cadastrar_usuarios.module.css"
import { NavLink } from "react-router-dom"

export function Cadastrar_Usuario() {
    return (
        <>
        <h1 className={styles.titulo}>Cadastrar Usuários</h1>
        <section className={styles.formulario}>
          <form>
            <div>
              <label>Nome</label>
              <input type="text" required></input>
            </div>
            <div>
              <label>CPF</label>
              <input type="text" required></input>
            </div>
            <div>
              <label>Cargo</label>
              <Drop_cadastro />
            </div>
            <div>
              <label>Email</label>
              <input type="email" required></input>
            </div>
            <div>
              <label>Senha</label>
              <input type="password" required></input>
            </div>
            <div>
              <NavLink to="/Lista_Usuarios"><button type="submit" className={styles.botao1}>Cadastrar</button></NavLink>
              <NavLink to="/Lista_Usuarios"><button type="submit" className={styles.botao}>Cancelar</button></NavLink>
            </div>
          </form>
        </section>
        </>
    )
}