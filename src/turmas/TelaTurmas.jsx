import styles from './telaturmas.module.css'
import { PlusCircle } from 'phosphor-react'
import { useEffect, useState } from 'react'
import { NavLink, useFetcher } from "react-router-dom"


export function TelaTurmas() {

    function carregaTurma(){
        const url = "http://localhost:3000/turmas/listarturmas"
        const config = {
            method: "GET"
        }
        fetch(url, config)
            .then(response => response.json() .then((TurmasArray) => {
                console.log(TurmasArray)
                setTurma(TurmasArray)
            }))
    }

    const [turma, setTurma] = useState('')
    useEffect(()=> { 
        carregaTurma()
    }, [])     
    

    function handleOnClickDeletar(idTurma){
        const url = `http://localhost:3000/turmas/${idTurma}`
        const config = {
            method: "DELETE"
        }
        fetch(url, config)
            .then(response => {
                carregaTurma()
            })
    }

    return(
        <div className={styles.center}>
            <section className={styles.titulo}>
                <h2>Turmas</h2>
                <NavLink to= '/Cadastrar_Turmas'>
                    <button type="submit" className={styles.buttonTurma} >
                        <PlusCircle size={24} />
                        <span>Turma</span>
                    </button>
                </NavLink>
            </section>
            <section className={styles.content}>

            {   turma &&
                turma.map((turmaDado, index)=> {
                    return(
                        <div key={index} className={styles.card}>
                        <div className={styles.info}>
                            <p>Turma: {turmaDado.numero}/{turmaDado.ano}</p>
                            <p>Ano: {turmaDado.ano}</p>
                            <p>Séria: {turmaDado.serie}</p>
                            <p>N° de alunos: 00</p>
                        </div>
                        <div className={styles.cardButtons}>
                            <NavLink><button type="submit" className={styles.botao}>Ver</button></NavLink>
                            <NavLink to="/Editar_Turmas"><button type="submit" className={styles.botao}>Editar</button></NavLink>
                            <button onClick={()=> handleOnClickDeletar(turmaDado.numero) } className={styles.botao}>Excluir</button>
                        </div>
                    </div>
                    )
                } )
            }
                
            </section>
        </div>
    )
}