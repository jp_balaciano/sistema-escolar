import { BrowserRouter } from "react-router-dom"
import { Router } from "./Router"
import  "./global.css"

function App() {
  return (
    <>
    <BrowserRouter>
        <Router></Router>
    </BrowserRouter>
    </>
  )
}

export default App
