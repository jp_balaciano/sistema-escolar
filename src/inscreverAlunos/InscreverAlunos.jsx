import styles from './inscreveralunos.module.css'
import { NavLink } from 'react-router-dom'

export function InscreverAlunos() {
    return(
        <div>
            <h1 className={styles.title}>Inscrever Alunos</h1>
            <div className={styles.center}>
                <section className={styles.search}>
                    <div>
                        <label htmlFor="">Pesquisar Aluno</label>
                        <input type="text" />
                    </div>
                    <button type="submit">Pesquisa</button>
                </section>
                <div className={styles.alunos}>
                    <section className={styles.infoAluno}>
                        <div>
                            <p><span>Nome:</span> Nome do aluno</p>
                            <div>
                                <p>CPF: 000.000.000-00</p>
                                <p>Idade: 00</p>
                            </div>
                        </div>
                        <div>
                            <span>Inscrever</span>
                            <input type="checkbox" name="" id="" />
                        </div>
                    </section>
                    <section className={styles.infoAluno}>
                        <div>
                            <p><span>Nome:</span> Nome do aluno</p>
                            <div>
                                <p>CPF: 000.000.000-00</p>
                                <p>Idade: 00</p>
                            </div>
                        </div>
                        <div>
                            <span>Inscrever</span>
                            <input type="checkbox" name="" id="" />
                        </div>
                    </section>
                </div>
                <section className={styles.Buttons}>
                    <NavLink>
                        <button type="submit" className={styles.butConfirma}>Confirma</button>
                    </NavLink>
                    <NavLink>
                        <button type="submit" className={styles.butCncelar}>Cancelar</button>
                    </NavLink>
                </section>
            </div>
        </div>
    )
}