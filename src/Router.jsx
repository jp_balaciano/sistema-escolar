import { Routes, Route } from "react-router-dom"
import { BaseLayout } from "./BaseLayout"
import { TelaLogin } from "./login/TelaLogin"
import { Lista_Professores } from "./professores/Lista/Lista_professores"
import { Cadastrar_Professor } from "./professores/Cadastro/Cadastrar_professor"
import { Editar_professor } from "./professores/Editar/Editar_professor"
import { Lista_Usuarios } from "./Usuarios/Listagem/Lista_usuarios"
import { Cadastrar_Usuario } from "./Usuarios/Cadastro/Cadastrar_usuarios"
import { Editar_usuario } from "./Usuarios/Editar/Editar_usuario"
import { TelaTurmas } from "./turmas/TelaTurmas"
import { CadastrarTurma } from "./cadastrarTurmas/CadastrarTurma"
import { InscreverAlunos } from "./inscreverAlunos/InscreverAlunos"
import { EditarTurma } from "./editarTurma/EditarTurma"



export function Router() {
    return (
        <Routes>
            <Route path="/login" element={<TelaLogin/>} />
            <Route path="/" element={<BaseLayout/>}> 
                <Route path="/" element={<Lista_Professores/>}/> 
                <Route path="/Cadastrar_Professor" element={<Cadastrar_Professor/>}/> 
                <Route path="/Editar_Professor" element={<Editar_professor/>}/> 
                <Route path="/Lista_Usuarios" element={<Lista_Usuarios/>}/> 
                <Route path="/Cadastrar_Usuarios" element={<Cadastrar_Usuario/>}/> 
                <Route path="/Editar_Usuarios" element={<Editar_usuario/>}/> 
                <Route path="/Cadastrar_Turmas" element={<CadastrarTurma/>}/>
                <Route path="/Inscrever_Alunos" element={<InscreverAlunos/>}/>
                <Route path="/Editar_Turmas" element={<EditarTurma/>}/>
                <Route path="/turmas" element={<TelaTurmas/>}/>
            </Route>
        </Routes>
    )
}